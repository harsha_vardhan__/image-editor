package com.example.imageeditor;

import static com.example.imageeditor.MainActivity.tag;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;

public class BitmapHelper {

    Bitmap getBitmap(ImageView image) {
        BitmapDrawable draw = (BitmapDrawable) image.getDrawable();
        return draw.getBitmap();
    }


    //https://stackoverflow.com/questions/36493977/flip-a-bitmap-image-horizontally-or-vertically
    public static Bitmap createFlippedBitmap(Bitmap source, boolean xFlip, boolean yFlip) {
        Matrix matrix = new Matrix();
        matrix.postScale(xFlip ? -1 : 1, yFlip ? -1 : 1, source.getWidth() / 2f, source.getHeight() / 2f);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    void storeImage(String saveFileName, Bitmap bitmap, boolean flippedX, boolean flippedY, OnSaveCallback callback) {

        Thread t = new Thread(() -> {
            Bitmap image = createFlippedBitmap(bitmap, flippedX, flippedY);
            File pictureFile = getOutputMediaFile(saveFileName);
            if (pictureFile == null) {
                return;
            }
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                image.compress(Bitmap.CompressFormat.PNG, 90, fos);
                fos.close();
            } catch (Exception e) {
                callback.onSave(false);
            }
            callback.onSave(true);
        });
        t.start();
    }

    public static final String IMG_PATH = File.separator + "Pictures";

    private File getOutputMediaFile(String saveFileName) {

        String fileName = TextUtils.isEmpty(saveFileName) ? "" + System.currentTimeMillis() : saveFileName;
        String mPath = Environment.getExternalStorageDirectory().toString() + IMG_PATH;

        new File(mPath).mkdirs();
        mPath += File.separator + fileName + ".png";
        return new File(mPath);
    }

    void cropImage(ImageView image,boolean flippedX, boolean flippedY, float x, float y, int w, int h) {
        Bitmap bitmap = getBitmap(image);

        float factor = bitmap.getWidth() * 1f / image.getWidth();

        int left = scale((int) x, factor);
        int top = scale((int) y, factor);
        int width = scale(w, factor);
        int height = scale(h, factor);
        //quick fix to adjust boundaries
        if(left<0)left=0;
        if(top<0)top=0;
        if(left+width>bitmap.getWidth())width=bitmap.getWidth()-left;
        if((top+height)>bitmap.getHeight()) height = bitmap.getHeight() - top;

        //reset image rotation to get the user desired effect
        bitmap = createFlippedBitmap(bitmap, flippedX, flippedY);
        image.setRotationX(0);image.setRotationY(0);
        Bitmap resized = Bitmap.createBitmap(bitmap, left, top, width, height);
        image.setImageBitmap(resized);
    }

    int scale(int val, float factor) {
        return (int) (val * factor);
    }
}
