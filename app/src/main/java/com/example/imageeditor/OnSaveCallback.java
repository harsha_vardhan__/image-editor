package com.example.imageeditor;

public interface OnSaveCallback {
    public void onSave(boolean saved);
}
