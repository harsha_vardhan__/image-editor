package com.example.imageeditor;

import static com.example.imageeditor.MainActivity.makeToast;
import static com.example.imageeditor.MainActivity.tag;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import java.util.Timer;
import java.util.TimerTask;

public class EditorFragment extends Fragment implements OnSaveCallback {
    private String saveFileName;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity = (MainActivity) context;
    }

    MainActivity activity;
    BitmapHelper helper;
    ImageView image, flipH, flipV, crop, info, save;
    int animValX = 0, animValY = 0, ANIM_DURATION = 1000;
    ProgressBar progressBar;
    ImageView tl, br;
    View overlay;
    float cropLimit;
    float tHeight = 0, tWidth;
    float top, bottom, left, right, valY, valX;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.editor, container, false);
        Bundle bundle = getArguments();
        helper = new BitmapHelper();
        String path = bundle.getString("path");
        assignViews(root);

        image.setImageURI(Uri.parse(path));
        info.setOnClickListener(v -> activity.dialog(helper.getBitmap(image)));
        save.setOnClickListener(v -> {
            customDialog();
        });
        image.setOnClickListener(v -> {

        });
        crop.setOnClickListener(v -> cropLogic());
        flipH.setOnClickListener(v -> animateY());
        flipV.setOnClickListener(v -> animateX());

        return root;
    }

    void assignViews(View root) {
        progressBar = root.findViewById(R.id.progress);
        save = root.findViewById(R.id.save);
        image = root.findViewById(R.id.image);
        info = root.findViewById(R.id.info);
        flipH = root.findViewById(R.id.flip_h);
        flipV = root.findViewById(R.id.flip_v);
        crop = root.findViewById(R.id.crop);
        overlay = root.findViewById(R.id.overlay);
        tl = root.findViewById(R.id.tl);
        br = root.findViewById(R.id.br);
        cropLimit = activity.getResources().getDimension(R.dimen.crop_image_length);

    }

    private void cropLogic() {
        if (overlay.getVisibility() == View.VISIBLE) {
            crop.setSelected(false);
            overlay.setVisibility(View.GONE);

            boolean flippedY = (animValX / 180) % 2 == 1;
            boolean flippedX = (animValY / 180) % 2 == 1;
            animValX = 0;
            animValY = 0;
            helper.cropImage(image, flippedX, flippedY, overlay.getX() - image.getX(), overlay.getY() - image.getY(), overlay.getWidth(), overlay.getHeight());
        } else {
            makeToast("Press again to crop", activity);
            overlay.setVisibility(View.VISIBLE);
            crop.setSelected(true);
            tl.setOnTouchListener((v, event) -> {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    tWidth = overlay.getWidth();
                    tHeight = overlay.getHeight();
                    left = event.getRawX();
                    top = event.getRawY();
                    valY = overlay.getY();
                    valX = overlay.getX();
                }
                if (event.getAction() == MotionEvent.ACTION_MOVE) {

                    float x = event.getRawX();
                    float y = event.getRawY();
                    ViewGroup.LayoutParams params = overlay.getLayoutParams();
                    float xDiff = left - x;
                    int newW = (int) (tWidth + xDiff);
                    if (newW > cropLimit) {
                        params.width = newW;
                        overlay.setX(valX + (x - left));
                    }
                    float yDiff = y - top;
                    int newH = (int) (tHeight - yDiff);
                    if (newH > cropLimit) {
                        params.height = newH;
                        overlay.setY(valY + (y - top));
                    }
                    overlay.setLayoutParams(params);
                }
                return true;
            });

            br.setOnTouchListener((v, event) -> {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    tWidth = overlay.getWidth();
                    tHeight = overlay.getHeight();
                    right = event.getRawX();
                    bottom = event.getRawY();
                    valY = overlay.getY();
                    valX = overlay.getX();
                }
                if (event.getAction() == MotionEvent.ACTION_MOVE) {

                    float x = event.getRawX();
                    float y = event.getRawY();
                    ViewGroup.LayoutParams params = overlay.getLayoutParams();
                    float xDiff = right - x;
                    int newW = (int) (tWidth - xDiff);
                    if (newW > cropLimit) {
                        params.width = newW;
                        overlay.setX(valX);
                    }
                    float yDiff = y - bottom;
                    int newH = (int) (tHeight + yDiff);
                    if (newH > cropLimit) {
                        params.height = newH;
                        overlay.setY(valY);
                    }
                    overlay.setLayoutParams(params);
                }
                return true;
            });
        }
    }

    void animateX() {
        animValX += 180;
        image.animate().setDuration(ANIM_DURATION).rotationX(animValX);
    }

    void animateY() {
        animValY += 180;
        image.animate().setDuration(ANIM_DURATION).rotationY(animValY);
    }


    private void saveMethod() {
        if (!activity.checkStorageperm()) return;
        boolean flippedY = (animValX / 180) % 2 == 1;
        boolean flippedX = (animValY / 180) % 2 == 1;
        progressBar.setVisibility(View.VISIBLE);
        helper.storeImage(saveFileName, helper.getBitmap(image), flippedX, flippedY, this);
    }

    private void customDialog() {
        LayoutInflater factory = LayoutInflater.from(activity);
        final View fileNameDialog = factory.inflate(R.layout.custom_dialog, null);
        EditText edit = fileNameDialog.findViewById(R.id.edit);
        final AlertDialog dialog = new AlertDialog.Builder(activity).create();
        dialog.setView(fileNameDialog);
        fileNameDialog.findViewById(R.id.confirm).setOnClickListener(v -> {
            saveFileName = edit.getText().toString();
            saveMethod();
            dialog.dismiss();
        });
        fileNameDialog.findViewById(R.id.cancel).setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.show();
    }

    @Override
    public void onSave(boolean saved) {
        activity.runOnUiThread(() -> {
            String message = saved ? "Saved successfully" : "error in saving";
            progressBar.setVisibility(View.GONE);
            makeToast(message, activity);
        });

    }
}